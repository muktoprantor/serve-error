package serveerror

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestInvalid(t *testing.T) {

	exp := strings.Join([]string{
		fmt.Sprintf("%d", http.StatusBadRequest),
		"user",
		"invalid password",
		"minimum limit is 5",
	}, separator)

	err := Invalid("user", "invalid password", "minimum limit is 5")

	if exp != err.Error() {
		t.Fatalf("TestInvalid: expected %v but got %v", exp, err)
	}

}

func TestServe(t *testing.T) {
	dd := []struct {
		name string
		err  Err
		exp  res
	}{
		{
			"Invalid",
			Invalid("user", "invalid password", "minimum limit is 5"),
			res{"user", "invalid password", "minimum limit is 5"},
		},
	}

	for _, tc := range dd {
		t.Run(tc.name, func(t *testing.T) {
			req, err := http.NewRequest("GET", "/error_check", nil)
			if err != nil {
				t.Fatal(err)
				return
			}

			rr := httptest.NewRecorder()
			handler := http.HandlerFunc(tc.err.Serve)

			handler.ServeHTTP(rr, req)

			if rr.Code != tc.err.Status {
				t.Errorf("expected status code is %v but got %v", tc.err.Status, rr.Code)
				return
			}

			resp := res{}
			json.NewDecoder(rr.Body).Decode(&resp)

			if resp.Where != tc.exp.Where {
				t.Errorf("expected valud for where is (%v) but got (%v)", tc.exp.Where, resp.Where)
			}
			if resp.Cause != tc.exp.Cause {
				t.Errorf("expected valud for cause is (%v) but got (%v)", tc.exp.Cause, resp.Cause)
			}
			if resp.Msg != tc.exp.Msg {
				t.Errorf("expected valud for msg is (%v) but got (%v)", tc.exp.Msg, resp.Msg)
			}

		})
	}
}
