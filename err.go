package serveerror

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
)

const separator string = "#$"

// Err provides generics error structure
type Err struct {
	Status int
	Where  string
	Cause  string
	Msg    string
}

type res struct {
	Where string `json:"where,omitempty"`
	Cause string `json:"cause,omitempty"`
	Msg   string `json:"msg,omitempty"`
}

func (e Err) toRes() res {
	return res{
		Where: e.Where,
		Cause: e.Cause,
		Msg:   e.Msg,
	}
}

func (e Err) Error() string {
	return strings.Join([]string{
		fmt.Sprintf("%d", e.Status),
		e.Where,
		e.Cause,
		e.Msg,
	}, separator)
}

// Serve send error as json response
func (e Err) Serve(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(e.Status)

	if err := json.NewEncoder(w).Encode(e.toRes()); err != nil {
		log.Printf("serveerror : Serve : %v", err)
	}

}

// Invalid returns Err with badrequest
func Invalid(where, cause, msg string) Err {
	return Err{
		Status: http.StatusBadRequest,
		Where:  where,
		Cause:  cause,
		Msg:    msg,
	}
}
